# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-03-01 16:59+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: =head1
#: ../dgit.1:3 ../dgit.7:2 ../dgit-user.7.pod:1 ../dgit-nmu-simple.7.pod:1
#: ../dgit-maint-native.7.pod:1 ../dgit-maint-merge.7.pod:1
#: ../dgit-maint-gbp.7.pod:1 ../dgit-maint-debrebase.7.pod:1
#: ../dgit-downstream-dsc.7.pod:1 ../dgit-sponsorship.7.pod:1
#: ../git-debrebase.1.pod:1 ../git-debrebase.5.pod:1
#, no-wrap
msgid "NAME"
msgstr ""

#. type: =head1
#: ../dgit.1:1470 ../dgit.7:23 ../dgit-user.7.pod:447
#: ../dgit-nmu-simple.7.pod:137 ../dgit-maint-native.7.pod:126
#: ../dgit-maint-merge.7.pod:491 ../dgit-maint-gbp.7.pod:136
#: ../dgit-maint-debrebase.7.pod:747 ../dgit-downstream-dsc.7.pod:352
#: ../dgit-sponsorship.7.pod:321 ../git-debrebase.1.pod:619
#: ../git-debrebase.5.pod:678
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:5 ../dgit-maint-native.7.pod:5
#: ../dgit-maint-merge.7.pod:5 ../dgit-maint-gbp.7.pod:5
#: ../dgit-maint-debrebase.7.pod:5 ../dgit-downstream-dsc.7.pod:5
#: ../git-debrebase.1.pod:10 ../git-debrebase.5.pod:5
msgid "INTRODUCTION"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:449 ../dgit-maint-native.7.pod:128
#: ../dgit-maint-merge.7.pod:493 ../dgit-maint-gbp.7.pod:138
msgid "dgit(1), dgit(7)"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:3
msgid "dgit - tutorial for package maintainers of Debian-native packages"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:7
msgid ""
"This document describes elements of a workflow for using B<dgit> to maintain "
"a Debian package that uses one of the native source formats (\"1.0\" & \"3.0 "
"(native)\")."
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:15
msgid "We expect that your git history is fast-forwarding."
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:19
msgid ""
"You should be prepared to tolerate a small amount of ugliness in your git "
"history in the form of merges which stitch the dgit-generated archive view "
"into your maintainer history."
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:25
msgid ""
"This is to handle uploads that were not made with dgit, such as the uploads "
"you made before switching to this workflow, or NMUs."
msgstr ""

#. type: =head2
#: ../dgit-maint-native.7.pod:31
msgid "Benefits"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:37 ../dgit-maint-gbp.7.pod:16
#: ../dgit-maint-debrebase.7.pod:38
msgid ""
"Benefit from dgit's safety catches.  In particular, ensure that your upload "
"always matches exactly your git HEAD."
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:42
msgid "Provide a better, more detailed history to downstream dgit users."
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:48
msgid "Incorporate an NMU with one command."
msgstr ""

#. type: =head1
#: ../dgit-maint-native.7.pod:52
msgid "FIRST PUSH WITH DGIT"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:54
msgid "You do not need to do anything special to your tree to push with dgit."
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:57
msgid "Simply prepare your git tree in the usual way, and then:"
msgstr ""

#. type: verbatim
#: ../dgit-maint-native.7.pod:61
#, no-wrap
msgid ""
"    % dgit -wgf sbuild -A -c sid\n"
"    % dgit -wgf --overwrite push\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:66
msgid "(Do not make any tags yourself: dgit push will do that.)"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:68
msgid ""
"You may use B<dgit pbuilder> or B<dgit cowbuilder> instead of B<dgit "
"sbuild>; see dgit(1) for the syntax of those subcommands."
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:72
msgid ""
"The --overwrite option tells dgit that you are expecting that your git "
"history is not a descendant of the history which dgit synthesised from the "
"previous non-dgit uploads."
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:77
msgid ""
"dgit will make a merge commit on your branch but without making any code "
"changes (ie, a pseudo-merge)  so that your history, which will be pushed to "
"the dgit git server, is fast forward from the dgit archive view."
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:85
msgid ""
"Alternatively, if this was the first ever dgit push of the package, you can "
"avoid this merge commit by passing C<--deliberately-not-fast-forward> "
"instead of C<--overwrite>.  This avoids introducing a new origin commit into "
"your git history."
msgstr ""

#. type: =head1
#: ../dgit-maint-native.7.pod:93
msgid "SUBSEQUENT PUSHES"
msgstr ""

#. type: verbatim
#: ../dgit-maint-native.7.pod:97
#, no-wrap
msgid ""
"    % dgit -wgf push\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:101 ../dgit-maint-native.7.pod:111
msgid "That's it."
msgstr ""

#. type: =head1
#: ../dgit-maint-native.7.pod:103
msgid "INCORPORATING AN NMU"
msgstr ""

#. type: verbatim
#: ../dgit-maint-native.7.pod:107 ../dgit-maint-merge.7.pod:484
#, no-wrap
msgid ""
"    % dgit pull\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:113
msgid "Or, if you would prefer to review the changes, you can do this:"
msgstr ""

#. type: verbatim
#: ../dgit-maint-native.7.pod:118
#, no-wrap
msgid ""
"    % dgit fetch\n"
"    % dgit diff HEAD..dgit/dgit/sid\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-maint-native.7.pod:123
msgid ""
"If you do not merge the NMU into your own git history, the next push will "
"then require I<--overwrite>."
msgstr ""
