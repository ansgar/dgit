-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: pari-extra
Binary: pari-extra
Architecture: all
Version: 3-1
Maintainer: Bill Allombert <ballombe@debian.org>
Standards-Version: 3.9.2.0
Build-Depends: debhelper (>= 5)
Package-List: 
 pari-extra deb math optional
Checksums-Sha1: 
 ff281e103ab11681324b0c694dd3514d78436c51 121 pari-extra_3.orig.tar.gz
 080078dbc51e4194d209cb5abe57e2b25705fcaa 2358 pari-extra_3-1.diff.gz
Checksums-Sha256: 
 ac1ef39f9da80b582d1c0b2adfb09b041e3860ed20ddcf57a0e922e3305239df 121 pari-extra_3.orig.tar.gz
 bf4672acd5302b9eebee2f3bf5269022279e531204d7172b8761bb10fae3517a 2358 pari-extra_3-1.diff.gz
Files: 
 76bcf03be979d3331f9051aa88439b8b 121 pari-extra_3.orig.tar.gz
 02a39965adb84da9b3e6b5c5a0a4c378 2358 pari-extra_3-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk5CvdoACgkQeDPs8bVESBX0mACeK3Yf9y22T2b6tw8eVQ8XSYxH
ix4AoJJ3jrGJ4HXJNv/wbvmvBkkybvYJ
=hkic
-----END PGP SIGNATURE-----
