=head1 NAME

dgit - tutorial for package maintainers already using git-buildpackage(1)

=head1 INTRODUCTION

This document explains how B<dgit> can be incorporated into a
git-buildpackage(1) package-maintenance workflow.  This should be read
jointly with git-buildpackage(1)'s documentation.  Some reasons why
you might want to incorporate B<dgit> into your existing workflow:

=over 4

=item

Benefit from dgit's safety catches.  In particular, ensure that your
upload always matches exactly your git HEAD.

=item

Provide a better, more detailed git history to downstream dgit users,
such as people using dgit to do an NMU (see dgit-nmu-simple(7) and
dgit-user(7)).

=back

Note that we assume a patches-unapplied repository: the upstream
source committed to the git repository is unpatched.
git-buildpackage(1) can work with patched-applied repositories, but is
normally used with patches-unapplied.

=head1 GIT CONFIGURATION

If you have configured an I<export-dir> in your gbp.conf, you should
tell dgit about it:

=over 4

    % git config --global dgit.default.build-products-dir /home/spwhitton/build-area

=back

If you run

=over 4

    % git config dgit.default.quilt-mode gbp

=back

in your repository, you can omit I<--gbp> wherever it occurs below.

Note that this does require that you always work from your gbp master
branch, never the dgit patches-applied branch.

=head1 BUILDING

You can perform test builds like this:

=over 4

    % dgit [--include-dirty] gbp-build [OPTIONS]

=back

where I<--include-dirty> is needed for testing uncommitted changes, and
I<OPTIONS> are any further options to be passed on to
gbp-buildpackage(1).

If you are doing a source-only upload, you do not need to prepare a
I<_source.changes>, as B<dgit push-source> will take of that on your
behalf.

If you need to include binaries with your upload, you will probably
want to use sbuild(1), pbuilder(1) or cowbuilder(1):

=over 4

    % dgit --rm-old-changes --gbp sbuild

=back

replacing 'sbuild' with 'pbuilder' or 'cowbuilder' if appropriate.

We use I<--rm-old-changes> to ensure that there is exactly one changes
file corresponding to this package, so we can be confident we're
uploading what we intend (though B<dgit push> will do some safety
checks).

Note that none of the commands in this section are required to upload
with dgit.  You can invoke gbp-buildpackage(1), pbuilder(1),
cowbuilder(1) and sbuild(1) directly.  However, the defaults for these
tools may leave you with something that dgit will refuse to upload
because it doesn't match your git HEAD.

As a general rule, leave all signing and tagging to dgit.

=head1 UPLOADING

Don't use I<--git-tag>: B<dgit push> will do this for you.  To do a
source-only upload:

=over 4

    % dgit --gbp push-source

=back

or if you need to include binaries,

=over 4

    % dgit --gbp push

=back

This will push your git history to the dgit-repos, but you probably
want to follow it up with a push to salsa.

You will need to pass I<--overwrite> if the previous upload was not
performed with dgit.

If this is first ever dgit push of the package, consider passing
I<--deliberately-not-fast-forward> instead of I<--overwrite>.  This
avoids introducing a new origin commit into the dgit view of your git
history.  (This origin commit would represent the most recent non-dgit
upload of the package, but this should already be represented in your
git history.)

=head1 INCORPORATING NMUS

B<dgit pull> can't yet incorporate NMUs into patches-unapplied gbp
branches.  You can just apply the NMU diff the traditional way.  The
next upload will require I<--overwrite>.

=head1 SEE ALSO

dgit(1), dgit(7)

=head1 AUTHOR

This tutorial was written and is maintained by Sean Whitton <spwhitton@spwhitton.name>.
